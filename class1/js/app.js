(function () {
  'use strict';

  var app;

  /**
   * Define 'todoApp' module
   */
  app = angular.module('todoApp', []);

  /** 
   * Define 'TodoCtrl' controller
   */
  app.controller('TodoCtrl', function ($scope) {
    /**
     * Method for add item in items array
     */
    function addItem () {
      $scope.items.push({
        name: $scope.newItem.name,
        done: false
      });
      $scope.newForm = {};
    }

    /** 
     * Method for remove an item from items array
     * @param  {Object} [removeItem] item reference to remove
     */
    function removeItem (removeItem) {
      $scope.items.some(function (item, index) {
        if (item.name === removeItem.name) {
          $scope.items.splice(index, 1);
          return true;
        }
        return false;
      });
    }

    /**
     * Method for count all dones todos items
     * @return {Number} count of done's todo items
     */
    function countDoneItems () {
      return $scope.items.reduce(function (count, item) {
        if (item.done) {
          count++;
        }
        return count;
      }, 0);
    }

    // console.log(TodoService);

    $scope.newItem = {}; //define new item object

    //define two items for example todo app
    $scope.items = [{
      name: 'example item 1',
      done: false
    }, {
      name: 'example item 2',
      done: true
    }];

    //defines scope functions
    $scope.addItem = addItem;
    $scope.removeItem = removeItem;
    $scope.countDoneItems = countDoneItems;
  })

})();
