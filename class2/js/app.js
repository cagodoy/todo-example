(function () {
  'use strict';

  var app;

  /**
   * Define 'todoApp' module
   */
  app = angular.module('todoApp', []);

})();
