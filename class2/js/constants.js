/**
 * Use 'todoApp' module
 */
var app = angular.module('todoApp');

/** 
 * Define 'ENV' constants
 */
app.constant('ENV', {
  apiURL: 'https://banchileexampleapi.herokuapp.com/api/todos/'
});
