/**
 * Define 'todoApp' module
 */
app = angular.module('todoApp');

/** 
 * Define 'TodoCtrl' controller
 */
app.controller('TodoCtrl', function ($scope, Todo) {
  /**
   * Method for add item in items array
   */
  function addItem () {
    var data = {
      name: $scope.newItem.name,
      done: false
    };

    Todo.post(data)
      .then(function (result) {
        $scope.items.push(result.data);
      });
  }

  /** 
   * Method for remove an item from items array
   * @param  {Object} [removeItem] item reference to remove
   */
  function removeItem (removeItem) {
    Todo.delete(removeItem._id)
      .then(function (result) {
        $scope.items.some(function (item, index) {
          if (item._id === removeItem._id) {
            $scope.items.splice(index, 1);
            return true;
          }
          return false;
        });
      })
  }

  /**
   * Method for count all dones todos items
   * @return {Number} count of done's todo items
   */
  function countDoneItems () {
    // return $scope.items.reduce(function (count, item) {
    //   if (item.done) {
    //     count++;
    //   }
    //   return count;
    // }, 0);
  }

  $scope.newItem = {}; //define new item object

  //define two items for example todo app
  // $scope.items = [{
  //   name: 'example item 1',
  //   done: false
  // }, {
  //   name: 'example item 2',
  //   done: true
  // }];

  //defines scope functions
  $scope.addItem = addItem;
  $scope.removeItem = removeItem;
  $scope.countDoneItems = countDoneItems;

  Todo.get()
    .then(function (result) {
      $scope.items = result.data;
    });
    
});
