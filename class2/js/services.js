/**
 * Use 'todoApp' module
 */
var app = angular.module('todoApp');

/** 
 * Define 'Todo' factory service
 */
app.factory('Todo', function ($http, ENV) {
  var apiURL = ENV.apiURL;

  return {
    get: function () {
      return $http.get(apiURL);
    },
    post: function (data) {
      return $http.post(apiURL, data);
    },
    delete: function (id) {
      return $http.delete(apiURL + id);
    }
  }
});
